#' @title Rain Gauge analysis
#' @usage  rain gauge analysis, single outlier, spatiale correction and series outlier
#' @description Fonction pour l'analyse des single et series outliers avec une correction des tendance spatiales
#' @param rawData Table d'entrée des données
#' @param experimentName présentaiton des données
#' @param spatialCorrection FALSE, "fixe" or "random"
#' @param confIntSize A numeric value defining the confidence interval. the level to calculate the confidence interval. Increase confIntSize to exclude less outliers
#' @param nnLocfitSingleOutlier Single Outlier Detection : A numeric value defining the constant component of the smoothing parameter nn. the constant of the smoothing parameter. Increase nnLocfitSingleOutlier to have a very smooth curve
#' @param spar smoothing parameter, typically (but not necessarily) in (0,1](0,1]. When spar is specified, the coefficient \lambdaλ of the integral of the squared second derivative in the fit (penalized log likelihood) criterion is a monotone function of spar, see the details below. Alternatively lambda may be specified instead of the scale free spar=ss.
#' @param stepTP step in time point.
#' @param quiet Should printed progress messages be suppressed?
#' @param knots Series Outlier Detection : The number of knots to use when fitting the spline
#' @param minNoTP Series Outlier Detection : The minimum number of time points for which data should be available for a plant
#' @param thrCor Series Outlier Detection : A numerical value used as threshold for determining outliers based on correlation between plots.
#' @param thrPca Series Outlier Detection : A numerical value used as threshold for determining outliers based on angles (in degrees) between PCA scores.
#' @param thrSlope Series Outlier Detection : A numerical value used as threshold for determining outliers based on slopes.
#' @return une liste de table de données. les nom des tables est présenter dans l lfux d'analyse des capterus.
#' @export
#' @examples fInstruRainGaugeAnalysis(...)
#' @name fInstruRainGaugeAnalysis

#### exmple ####
if(FALSE){
  dfinstru <- read.csv(file = "C:/Users/leveaust/Documents/Projets/C4Future/workflow/2023-02-20/table_rawData_instrumentation_C4Future_2023-02-20.csv")
  sensorType <- "TippingBucketRainGaugeWithMechanicalCorrectionTbrgMc"
  trial <- "INRdia2022"
  rawData = subset(x = dfinstru,subset = SensorType %in% sensorType & EID %in% trial & !Value %in% 0)
  experimentName = paste(sensorType,trial)

  levelID = "TREAT_ID"

  variableName <- list(
    EID = "EID",
    TREAT_ID = "TREAT_ID",
    genotype = "Genotype",
    Plot = "Plot",
    SensorName = "SensorName",
    Replicate = "Replicate",
    x = "x",
    y = "y",
    timePoint = "Date_Time_Round",
    Value = "Value",
    THTc = "THTc"
  )

  input = list(
    CumulativeRainGauge=FALSE
  )
  singleOutlier = list(
    calculate = TRUE,
    confIntSize = 10,
    nnLocfitSingleOutlier = 0.1
    )
  adjustData = list(
    calculate = FALSE, # adjustData
    nnLocfitAdjustData = 0.2
  )
  spatialTrends = list(
    calculate = FALSE, #spatialCorrection,
    stepTP = 12,
    quiet = FALSE
  )
  serieOutlier = list(
    calculate = FALSE, #serieOutlier
    knots = 50,
    minNoTP =  0.8,
    thrCor = 0.8,
    thrPca = 30,
    thrSlope = 0.7
  )
  output = list(
    CumulativeRainGauge=TRUE
  )

}

#### Function ####
fInstruRainGaugeAnalysis <- function(
    rawData,
    experimentName,
    levelID = "TREAT_ID",
    variableName = list(
      EID = "EID",
      TREAT_ID = "TREAT_ID",
      genotype = "Genotype",
      Plot = "Plot",
      SensorName = "SensorName",
      Replicate = "Replicate",
      x = "x",
      y = "y",
      timePoint = "Date_Time_Round",
      Value = "Value",
      THTc = "THTc"
    ),
    input = list(
      CumulativeRainGauge=FALSE
    ),
    singleOutlier = list(
      calculate = TRUE,
      confIntSize = 3,
      nnLocfitSingleOutlier = 0.1
    ),
    adjustData = list(
      calculate = FALSE, # adjustData
      nnLocfitAdjustData = 0.2
    ),
    spatialTrends = list(
      calculate = FALSE, #spatialCorrection,
      stepTP = 12,
      quiet = FALSE
    ),
    serieOutlier = list(
      calculate = FALSE, #serieOutlier
      knots = 50,
      minNoTP =  0.8,
      thrCor = 0.8,
      thrPca = 30,
      thrSlope = 0.7
    ),
    output = list(
      CumulativeRainGauge=TRUE
    )
  ){
  #### begining ####
  cat("\n Analysis : ",experimentName,":")
  #### parameter compiliation

  parameters <- list(
    experimentName = experimentName,
    input = input,
    singleOutlier = singleOutlier,
    adjustData = adjustData,
    spatialTrends = spatialTrends,
    serieOutlier = serieOutlier,
    output=output
    )

  #### Initialization  ####
  cat("\n 0. Initialization : data organization")

  #### changer les nom des données
  # rawData <- rawData[,as.character(unlist(variableName))]
  # names(rawData) <- names(variableName)

  #### vérification qu'iil est bien une vaeur par timep point
  rawData <- aggregate(x = list(Value = rawData[,variableName$Value],
                                THTc = rawData[,variableName$THTc]),
                       by = list(EID = rawData[,variableName$EID],
                                 TREAT_ID = rawData[,variableName$TREAT_ID],
                                 genotype = rawData[,variableName$genotype],
                                 Plot = rawData[,variableName$Plot],
                                 SensorName = rawData[,variableName$SensorName],
                                 Replicate = rawData[,variableName$Replicate],
                                 x = rawData[,variableName$x],
                                 y = rawData[,variableName$y],
                                 timePoint = rawData[,variableName$timePoint]
                                 ),
                       FUN = function(x)mean(x,na.rm=T))
  #### mettre les valeurs de time point en character
  rawData$timePoint <- as.character(x = rawData$timePoint,tz = "UTC")

  #### Nombre de capteur par plot ####
  cat("\n 0. Initialization : sensor organisation")
  plots <- sort(unique(rawData$Plot))
  iplot <- sample(x = plots,size = 1) # for development
  for (iplot in plots) {
    iSensorNb <- subset(x = rawData,subset = Plot %in% iplot,"SensorName",drop=T)
    iSensorNb <- unique(as.character(iSensorNb))
    if(length(iSensorNb) > 1){
      isensor <- sample(2:length(iSensorNb),1) # for development
      for (isensor in 2:length(iSensorNb)) {
        rawData[rawData$SensorName%in%iSensorNb[isensor],"y"] =
          rawData[rawData$SensorName%in%iSensorNb[isensor],"y"] + 1
        rm(isensor)
      }
    }
    rm(iSensorNb,iplot)
  }
  rm(plots)

  # Vérificaiton il est bien une coordonnnées x et y  unique par nom de capteur.
  idf <- aggregate(list(x = rawData$x, y = rawData$y),list(SensorName=rawData$SensorName),unique)
  idf$xy <- paste0(idf$x,idf$y)
  idf$duplicated <- duplicated(idf$xy)
  if(any(idf$duplicated)){
    cat("\n There is at least one sensor that does not have a unique x and y coordinate")
    print(idf)
  }
  rm(idf)

  # vérifier par capteurs qu'il y a bien une position unique x et y
  idf <- rawData
  idf$xy <- paste(idf$x,idf$y,sep="_")
  idf <- aggregate(cbind(xy)~SensorName,idf,function(x)length(unique(x)))
  idf$check <- idf$xy>1
  if(any(idf$check)){
    cat("\n There is at least one sensor that does not have a unique x and y coordinate, cf. table : \n")
    print(idf[idf$check,])
    if(spatialCorrection %in% c("fixe","random"))
      stop("There must be a unique coordinate per sensor for the correction of spatial trends\n")
  }
  rm(idf)

  #### Cumulative
  if(parameters$input$CumulativeRainGauge)
  for (iplot in unique(rawData$Plot)) {
    rawData[rawData$Plot%in%iplot,"Value"] <- cumsum(rawData[rawData$Plot%in%iplot,"Value"])
    rm(iplot)
  }

  #### OUTLIE DETCTION : SINGLE OBSERVATION ####
  cat("\n 1. Outlier detection - single observation :",parameters$singleOutlier$calculate)

  if(parameters$singleOutlier$calculate){
    singleData <- data.frame(NULL)
    ilevelID <- sample(x = unique(rawData[,levelID]),1)
    for(ilevelID in sort(unique(rawData[,levelID]))){
      #### Create a TP object containing the data from the rawData
      irawDataTP <- createTimePoints(dat = rawData[rawData[,levelID]%in%ilevelID,],
                                    experimentName = experimentName,
                                    genotype = "genotype",
                                    timePoint = "timePoint",
                                    repId = "Replicate",
                                    plotId = "Plot",
                                    rowNum = "y",
                                    colNum = "x",
                                    addCheck = F)
      #### compute detection Single Outlier
      isingleData <- detectSingleOutRG(
        TP = irawDataTP,
        trait = "Value",
        group = "TREAT_ID",
        confIntSize = parameters$singleOutlier$confIntSize,
        nnLocfit = parameters$singleOutlier$nnLocfitSingleOutlier
        )
      singleData <- rbind(singleData,isingleData)
      rm(ilevelID,irawDataTP,isingleData)

      # singleData <- detectSingleOut(TP = irawDataTP,
      #                               trait = "Value",
      #                               plotIds = "plotId",
      #                               confIntSize = parameters$singleOutlier$confIntSize,
      #                               nnLocfit = parameters$singleOutlier$nnLocfitSingleOutlier)

    }





  }else{
    singleData <- rawData
    singleData$plotId <- singleData$Plot
  }

  #### add tht
  # singleData$THTc   <- dfththourly[match(x = singleData$timePoint,table = dfththourly$date),"THTc_hourAt20Degree"]

  #### Ajustement on data ####
  if(parameters$adjustData$calculate){
    #### fit a smmoooth spline with out outlier
    adjustData <- data.frame(NULL)
    plots <- unique(singleData$plotId)
    iplot <- sample(plots,1)
    for (iplot in plots) {
      ### vertion THTc
      # idf <- subset(singleData,plotId %in% iplot & outlier %in% 0)
      # plot(Value~THTc,idf,col=cbPalette[1],main = iplot)
      # m <- smooth.spline(x = idf$THTc,y = idf$Value,spar = 0.3)
      # points(x = m$x,y=m$y,col=cbPalette[2])
      # tht_est <- range(idf$THTc)
      # tht_est <- seq(from=tht_est[1],to = tht_est[2],by = 2)
      # p <- predict(object = m,x = tht_est,deriv = 0)
      # lines(x = p$x,y=p$y,col=cbPalette[3],lwd=2)

      ### vertion date
      idf <- subset(singleData,plotId %in% iplot & outlier %in% 0)

      #### smooth.spline ####
      # if(graphAdjustData) plot(Value~timePoint,idf,col=cbPalette[1],main = iplot)
      # m <- smooth.spline(x = idf$timePoint,y = idf$Value,spar = nnLocfitAdjustData)
      # # m$x <- as.POSIXct(x = m$x,tz = "UTC",origin = "1970-01-01 00:00.00")
      # if(graphAdjustData) points(x = m$x,y=m$y,col=cbPalette[2])
      # tht_est <- range(idf$timePoint)
      # tht_est <- range(m$x)
      # tht_est <- seq(from=tht_est[1],to = tht_est[2],by = 60*60)
      # p <- predict(object = m,x = tht_est,deriv = 0)
      # if(graphAdjustData) lines(x = p$x,y=p$y,col=cbPalette[3],lwd=2)
      # # add in table
      # adjustData <- rbind(adjustData,
      #                     data.frame(
      #                       plotId = iplot,
      #                       timePoint = p$x,
      #                       adjustValue = p$y
      #                     ))

      #### loc fit ####

      tht_est <- range(idf$timePoint)
      tht_est <- seq(from=tht_est[1],to = tht_est[2],by = 60*60)
      fitMod <-  locfit::locfit(formula = Value ~ locfit::lp(timePoint,
                                                             nn = parameters$adjustData$nnLocfitAdjustData,
                                                             deg = 2),
                                data = idf)
      pfitMod <- predict(fitMod, newdata = tht_est, se.fit = TRUE)
      # add in table
      adjustData <- rbind(adjustData,
                           data.frame(
                             plotId = iplot,
                             timePoint = tht_est,
                             adjustValue = pfitMod$fit,
                             adjustValueSE = pfitMod$se.fit
                           ))





    }
  }else{
    adjustData <- singleData
  }

  #### add metadata ####
  {
    adjustData$plotId <- as.factor(adjustData$plotId)
    adjustData$timePoint <- as.POSIXct(x = adjustData$timePoint,tz = "UTC",origin = "1970-01-01 00:00.00")
    # adjustData$SensorName <- rawData[match(adjustData$plotId,rawData$Plot),"SensorName"]
    adjustData$Plot <- rawData[match(adjustData$plotId,rawData$Plot),"Plot"]
    adjustData$Treatment <- rawData[match(adjustData$plotId,rawData$Plot),"TREAT_ID"]
    adjustData$Genotype <- rawData[match(adjustData$plotId,rawData$Plot),"genotype"]
    adjustData$Replicate <- rawData[match(adjustData$plotId,rawData$Plot),"Replicate"]
    adjustData$x <- rawData[match(adjustData$plotId,rawData$Plot),"x"]
    adjustData$y <- rawData[match(adjustData$plotId,rawData$Plot),"y"]
  }


  #### SPATIAL DATA ####
  cat("\n 2. Spatial Trend Correction :",parameters$spatialTrends$calculate)
  if(parameters$spatialTrends$calculate == "fixe"){
    # Crétion d'un génotpes X treatment
    adjustData$GenotypeXTreatment <- paste(adjustData$Genotype,adjustData$Treatment,sep = "_")
    # Create an object of class TP.
    adjustDataTP <- createTimePoints(dat = adjustData,
                                     experimentName = experimentName,
                                     genotype = "GenotypeXTreatment",
                                     timePoint = "timePoint",
                                     repId = "Replicate",
                                     plotId = "plotId",
                                     rowNum = "y",
                                     colNum = "x",
                                     addCheck = FALSE)
    # extration du nombre de treatmentXReplicateXgenotype dans la tables
    dfTP <- adjustData
    dfTP$ID <- paste(adjustData$Genotype,dfTP$Treatment,dfTP$Replicate,sep=".")
    dfTP <- aggregate(ID~timePoint,dfTP,function(x)length(unique(x)))
    dfTP$timePoint <- as.character(dfTP$timePoint)
    # Extract the time points table and add the number of row by time point
    timepoint <- getTimePoints(adjustDataTP)
    # compilation des données
    timepoint <- merge.data.frame(x = timepoint,
                                  y = dfTP,
                                  by = "timePoint",
                                  all = T)
    # On récupére uniquement les timepoint avec plus de un ID = scenario X Replicate X genotypes.
    dfTP <- aggregate(timeNumber~ID,timepoint,length)
    timepoint <- subset(timepoint,ID >= dfTP[dfTP$timeNumber%in% max(dfTP$timeNumber,na.rm=T),"ID"])
    # remove dfTP
    rm(dfTP)
    #### Fit a model with SpATS for three time points.
    adjustDataTPSP <- fitModels(TP = adjustDataTP,
                                trait = "adjustValue",
                                # timePoints = timepoint[match("2022-06-30 06:00:00",timepoint$timePoint),"timeNumber"],
                                # timePoints = timepoint$timePoint,
                                timePoints = seq(from = min(timepoint$timeNumber),
                                                 to = max(timepoint$timeNumber),
                                                 by= parameters$spatialTrends$stepTP),
                                # extraFixedFactors = c("Treatment"), # ne fonctionne pas, homogénéise les Treatments
                                # extraFixedFactors = "repId", # ne fonctionne pas : Error in SpATS
                                # geno.decomp = c("repId","Treatment"), # Error in if (dla < control$tolerance) break :
                                # geno.decomp = c("Treatment"),
                                geno.decomp = NULL,
                                what = c("fixe"),
                                engine = c("SpATS"),
                                useCheck = FALSE,
                                useRepId = FALSE,
                                spatial = FALSE,
                                quiet = parameters$spatialTrends$quiet
    )
    # Summary function for fitMod objects
    summary(adjustDataTPSP)
    ## Extract the corrected values for one time point:
    adjustDataSP <- getCorrected(adjustDataTPSP)
    # ajouter les numéros de plots dans la tables
    adjustDataSP$Genotype  <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Genotype"]
    adjustDataSP$Plot <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Plot"]
    adjustDataSP$Treatment <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Treatment"]
    adjustData <- adjustDataSP
  }
  if(parameters$spatialTrends$calculate == "random"){
    # Create an object of class TP.
    adjustDataTP <- createTimePoints(dat = adjustData,
                                     experimentName = experimentName,
                                     genotype = "Genotype",
                                     timePoint = "timePoint",
                                     repId = "Replicate",
                                     plotId = "plotId",
                                     rowNum = "y",
                                     colNum = "x",
                                     addCheck = FALSE)
    # extration du nombre de treatmentXReplicateXgenotype dans la tables
    dfTP <- adjustData
    dfTP$ID <- paste(adjustData$Genotype,dfTP$Treatment,dfTP$Replicate,sep=".")
    dfTP <- aggregate(ID~timePoint,dfTP,function(x)length(unique(x)))
    dfTP$timePoint <- as.character(dfTP$timePoint)
    # Extract the time points table and add the number of row by time point
    timepoint <- getTimePoints(adjustDataTP)
    # compilation des données
    timepoint <- merge.data.frame(x = timepoint,
                                  y = dfTP,
                                  by = "timePoint",
                                  all = T)
    # On récupére uniquement les timepoint avec plus de un ID = scenario X Replicate X genotypes.
    dfTP <- aggregate(timeNumber~ID,timepoint,length)
    timepoint <- subset(timepoint,ID >= dfTP[dfTP$timeNumber%in% max(dfTP$timeNumber,na.rm=T),"ID"])
    # remove dfTP
    rm(dfTP)
    #### Fit a model with SpATS for three time points.
    adjustDataTPSP <- fitModels(TP = adjustDataTP,
                                trait = "adjustValue",
                                # timePoints = timepoint[match("2022-06-30 06:00:00",timepoint$timePoint),"timeNumber"],
                                # timePoints = timepoint$timePoint,
                                timePoints = seq(from = min(timepoint$timeNumber),
                                                 to = max(timepoint$timeNumber),
                                                 by= parameters$spatialTrends$stepTP),
                                # extraFixedFactors = c("Treatment"), # ne fonctionne pas, homogénéise les Treatments
                                # extraFixedFactors = "repId", # ne fonctionne pas : Error in SpATS
                                # geno.decomp = c("repId","Treatment"), # Error in if (dla < control$tolerance) break :
                                geno.decomp = c("Treatment"),
                                # geno.decomp = NULL,
                                what = c("random"),
                                engine = c("SpATS"),
                                useCheck = FALSE,
                                useRepId = FALSE,
                                spatial = FALSE,
                                quiet = parameters$spatialTrends$quiet
    )
    # Summary function for fitMod objects
    summary(adjustDataTPSP)
    ## Extract the corrected values for one time point:
    adjustDataSP <- getCorrected(adjustDataTPSP)
    # ajouter les numéros de plots dans la tables
    adjustDataSP$Genotype  <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Genotype"]
    adjustDataSP$Plot <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Plot"]
    adjustDataSP$Treatment <- rawData[match(x = adjustDataSP$plotId,rawData$SensorName),"Treatment"]
    adjustData <- adjustDataSP
  }
  #### add Average Genotype X scénario ####
  {
    trait <- ifelse(test = parameters$adjustData$calculate,yes = "adjustValue",no = "Value")
    iplot <- sample(x = sort(unique(adjustData$plotId)),size = 1)
    for (iplot  in sort(unique(adjustData$plotId))) {
      dfmean <- subset(adjustData,plotId %in% iplot)
      dfmean <- subset(adjustData,Treatment %in% unique(dfmean$Treatment) & Genotype %in% unique(dfmean$Genotype))
      dfmean[,"trait"] <-  dfmean[,trait]
      dfmean <- aggregate(x = trait~timePoint+Genotype+Treatment,
                          data = dfmean,
                          FUN = function(x)mean(x,na.rm=T))
      adjustData[adjustData$plotId %in% iplot,paste0(trait,"_mean")] <-
        dfmean[match(x = adjustData[adjustData$plotId %in% iplot,"timePoint"],
                     table = dfmean[,"timePoint"]),
               "trait"]
      rm(dfmean)
    }
    rm(iplot,trait)
  }

  #### OUTLIE DETCTION : SERIE OBSERVATION ####
  cat("\n 3. Outlier detection - serie observation : ", parameters$serieOutlier$calculate)
  if(parameters$serieOutlier$calculate){
    #### quelle trait on analyse ? ####
    trait <- ifelse(test = parameters$adjustData$calculate,
                    yes = "adjustValue",no = "Value")
    trait <- ifelse(test = parameters$spatialTrends$calculate,
                    yes = paste0(trait,"_corr"),
                    no = trait)
    #### Common period analysis
    idf <- aggregate(timePoint~plotId,adjustData,length)
    idf$commonPeriod <- idf$timePoint >= (max(idf$timePoint) * parameters$serieOutlier$minNoTP )
    idf
    commonData <- subset(adjustData,plotId%in%idf[idf$commonPeriod%in%TRUE,"plotId"])
    rm(idf)
    # add metadata ppour le fitspline modèle ci-dessous.
    commonData$genotype <- commonData$Genotype
    #### serie outlier analysis
    fit.spline <- fitSpline(inDat = commonData,
                            trait = trait,
                            genotypes = as.character(unique(commonData$Genotype)),
                            knots = parameters$serieOutlier$knots,
                            minNoTP =  parameters$serieOutlier$minNoTP * length(unique(commonData[["timePoint"]])))
    # Extracting the tables of predicted values and P-spline coefficients
    predDat <- fit.spline$predDat
    coefDat <- fit.spline$coefDat
    # regardons les données
    # head(predDat) ; head(coefDat)
    #### Series outliers computation
    ## The coefficients are then used to tag suspect time courses.
    serieData <- detectSerieOut(corrDat = commonData,
                                predDat = predDat,
                                coefDat = coefDat,
                                trait = trait,
                                genotypes = as.character(unique(commonData$genotype)),
                                geno.decomp = c("Treatment"),
                                thrCor = parameters$serieOutlier$thrCor,
                                thrPca = parameters$serieOutlier$thrPca,
                                thrSlope = parameters$serieOutlier$thrSlope)

    serieData

    # remove Series outlier on c
    # serieData <- removeSerieOut(dat = commonData,
    #                             serieOut = serieData,
    #                             traits = c("adjustValue", "adjustValue_corr","adjustValue_mean"))
    # serieData <- subset(commonData,!plotId%in% serieData$plotId)
    # voir la différence entr les plantes
    # Remove outlier series on adjust data with series data tagger
    # adjustDataOut <- subset(adjustData,plotId%in% unique(serieData$plotId))
    adjustData <- subset(adjustData,!plotId%in% unique(serieData$plotId))
    #### Avrage and mesdian by génotypes
    #### sans les outlier series identifier
    #### priod sensor complete
    rm(trait)
  }else{
    commonData <- parameters$serieOutlier$calculate
    serieData <- parameters$serieOutlier$calculate
  }

  #### average par date ####
  cat("\n 4. Output Data - Genotype X Scenario : Average")

  #### quelle trait on analyse ? ####
  # trait <- ifelse(test = parameters$adjustData$calculate,
  #                 yes = "adjustValue",no = "Value")
  # trait <- ifelse(test = parameters$spatialTrends$calculate,
  #                 yes = paste0(trait,"_corr"),
  #                 no = trait)

  if(parameters$spatialTrends$calculate %in% c("fixe","random")){
    if(parameters$adjustData$calculate){
      outputData <- aggregate(cbind(adjustValue,adjustValue_corr)~timePoint+Treatment,
                              adjustData,
                              function(x)mean(x,na.rm=T))
    }else{
      outputData <- aggregate(cbind(Value,Value_corr)~timePoint+Treatment,
                              adjustData,
                              function(x)mean(x,na.rm=T))
    }
  }else{
    if(parameters$adjustData$calculate){
      outputData <- aggregate(cbind(adjustValue)~timePoint+Treatment,
                              adjustData,
                              function(x)mean(x,na.rm=T))
    }else{
      outputData <- aggregate(cbind(Value)~timePoint+Treatment,
                              adjustData,
                              function(x)mean(x,na.rm=T))
    }
  }

  cat("\n 4. Output Data - cumulative value : ",parameters$output$CumulativeRainGauge)

  if(parameters$output$CumulativeRainGauge){
    trait <- ifelse(test = parameters$adjustData$calculate,
                    yes = "adjustValue",no = "Value")
    trait <- ifelse(test = parameters$spatialTrends$calculate,
                    yes = paste0(trait,"_corr"),
                    no = trait)
    # outputData[,"GenotypeXTreatment"] <- paste(outputData$Genotype,outputData$Treatment,sep = "_")
    for (iT in unique(outputData$Treatment)) {
      outputData[outputData$Treatment%in%iT,paste0("cum",trait)] <-
        cumsum(outputData[outputData$Treatment%in%iT,trait])
      rm(iT)
      }
    rm(trait)
    }


  #### all data compilation ####
  cat("\n 5. Data compilation, add thermal time abd metadata \n")
  allData <- list(experimentName=experimentName,
                  parameters = parameters,
                  inputData = rawData,
                  singleData = singleData,
                  adjustData = adjustData,
                  commonData = commonData,
                  serieData = serieData,
                  outputData = outputData)

  #### add thermal time ####
  allData$inputData$timePoint <- as.POSIXct(allData$inputData$timePoint,"UTC")
  allData$outputData$timePoint <- as.POSIXct(allData$outputData$timePoint,"UTC")
  allData$outputData$THTc   <- allData$inputData[match(x = allData$outputData$timePoint,
                                                       table = allData$inputData$timePoint),"THTc"]
  #### add meta data ####
  allData$outputData$EID   <- unique(allData$inputData$EID)
  allData$outputData$TREAT_ID   <- allData$inputData[match(x = allData$outputData$Treatment,
                                                           table = allData$inputData$TREAT_ID),"TREAT_ID"]

  # warning message
  cat("\n ")
  warnings()

  # return
  return(allData)

}
